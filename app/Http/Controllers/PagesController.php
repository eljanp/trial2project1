<?php

// namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// class PagesController extends Controller
// {
//     return view('master') 
// }



// namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// class PagesController extends Controller
// {
//     return view('pages.index')
// }



namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use \App\Post;
use \App\User;
use Illuminate\Support\Facades\Auth;
use Validator;



class PagesController extends Controller
{
    public function index()
    {
    	return view('pages.index');
    }

    public function show()

    {
    	return view('pages.show');
    }

    public function logout () {
    //logout user
    auth()->logout();
    // redirect to homepage
    return redirect('pages/home');

    } 

    public function detect() {
        
        $agent = new Agent();
    
    }
    public function home() {
        
  

        return view('pages.index');
    
    } 
   public function showAdmin()
       {
           $users = User::all();
           //dd($posts);

           // foreach ($posts as $post){
           // echo $post->title . "<br>";
           // }
           $data['users'] = $users;

           return view('pages.admin', $data);
       }

       public function edit($id)
       {

        $user = User::find($id);

        $data['roles'] = \App\Role::all();

        $data['user'] = $user;

        return view ('pages.edit',$data);

       }

        public function update(Request $request)
       {
        // dd($request);



        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'role_id' => 'required',
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/'.$request->id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($request->id);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role_id = $request->input('role_id');
        
        $user->save();

            return redirect('/admin');

        }

        public function delete($id)
        {
            if (Auth::user()->id != $id){
            User::destroy($id);
        }

        return redirect('/admin');

        }
}


//adminPage


 // public function showAdmin()
 //    {
 //        $users = Post::all();
 //        //dd($posts);

 //        // foreach ($posts as $post){
 //        // echo $post->title . "<br>";
 //        // }
 //        $data['users'] = $users;

 //        return view('pages.admin', $data);
 //    }

 //    public function show($id)
 //    {
 //        $post = Post::find($id);

 //        $data['comments'] = $post->comments;//comments after the post

 //        $data['post'] = $post;

 //        return view ('post.show', $data);
 //    }
 //    public function create()
 //    {
 //        return view('post.create');
 //    }
 //    public function edit($id)
 //    {

 //        $post = Post::find($id);

 //        $data['post'] = $post;

 //        return view ('post.edit',$data);

 //    }
 //    public function store(Request $request)
 //    {
 //        // dd($request);

 //        $validator = Validator::make($request->all(), [
 //        'title' => 'required|unique:posts|max:255',
 //        'body' => 'required',
 //        ]);

 //        if ($validator->fails()) {
 //        return redirect('post')
 //        ->withErrors($validator)
 //        ->withInput();
 //        }

 //        $post = new Post;

 //        $post->title = $request->input('title');
 //        $post->body = $request->input('body');
        
 //        if($post->save()) {

 //            return redirect('/posts');

 //        } else {


 //        }
    

 //    }


 //     public function update(Request $request, $id)
 //    {
 //        // dd($request);

 //        $validator = Validator::make($request->all(), [
        
 //            'title' => 'required|unique:posts|max:255',
 //            'body' => 'required',
 //        ]);

 //        if ($validator->fails()) {
 //            return redirect('post/'.$id.'/edit')
 //                ->withErrors($validator)
 //                ->withInput();
 //        }

 //        $post = Post::find($id);

 //        $post->title = $request->input('title');
 //        $post->body = $request->input('body');
        
 //        if($post->save()) {

 //            return redirect('/post/'.$id.'/edit');

 //            } 

 //        }
 //        public function storeComment(Request $request)
 //        {
            
 //            $comment = new \App\Comment;
 //            // echo Auth::user()->name;


 //            $comment->user_id = Auth::user()->id ;
 //            $comment->post_id = $request->input('post_id');
 //            $comment->body = $request->input('body');
            
 //            $comment->save();

 //            return redirect('/post/'.$request->input('post_id') );

 //        }


