<?php

namespace App\Http\Controllers;


use Validator;
use Illuminate\Http\Request;
use \App\Post;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    public function index()
    {
    	$posts = Post::all();
    	//dd($posts);

    	// foreach ($posts as $post){
    	// echo $post->title . "<br>";
    	// }
    	$data['posts'] = $posts;

    	return view('post.index', $data);
    }

    public function show($id)
    {
    	$post = Post::find($id);

    	$data['comments'] = $post->comments;//comments after the post

    	$data['post'] = $post;

    	return view ('post.show', $data);
    }
    public function create()
    {
    	return view('post.create');
    }
    public function edit($id)
    {

    	$post = Post::find($id);

    	$data['post'] = $post;

    	return view ('post.edit',$data);

    }
    public function store(Request $request)
    {
    	// dd($request);

		$validator = Validator::make($request->all(), [
		'title' => 'required|unique:posts|max:255',
		'body' => 'required',
		]);

		if ($validator->fails()) {
		return redirect('post')
		->withErrors($validator)
		->withInput();
		}

    	$post = new Post;

    	$post->title = $request->input('title');
    	$post->body = $request->input('body');
		
		if($post->save()) {

			return redirect('/posts');

		} else {


		}
	

    }


     public function update(Request $request, $id)
    {
    	// dd($request);

		$validator = Validator::make($request->all(), [
		
			'title' => 'required|unique:posts|max:255',
			'body' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('post/'.$id.'/edit')
				->withErrors($validator)
				->withInput();
		}

    	$post = Post::find($id);

    	$post->title = $request->input('title');
    	$post->body = $request->input('body');
		
		if($post->save()) {

			return redirect('/post/'.$id.'/edit');

			} 

		}
		public function storeComment(Request $request)
		{
			
			$comment = new \App\Comment;
			// echo Auth::user()->name;


			$comment->user_id = Auth::user()->id ;
			$comment->post_id = $request->input('post_id');
			$comment->body = $request->input('body');
			
			$comment->save();

			return redirect('/post/'.$request->input('post_id') );

		}
//admin page


}

