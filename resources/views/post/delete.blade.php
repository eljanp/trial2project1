@extends('layout.master')

@section('content')

<section>
	<!-- Contact Section -->
<div id="contact" class="page">
<div class="container">
    <!-- Title Page -->
    <div class="row">
        <div class="span12">
            <div class="title-page">
                <h2 class="title">Edit Post</h2>
               <!--  <h3 class="title-description">We’re currently accepting new client projects. We look forward to serving you.</h3> -->
            </div>
        </div>
    </div>
    <!-- End Title Page -->
    
    <!-- Contact Form -->
    <div class="row" >
    	<div class="span9">
        
        	<form method="post" id="contact-form" class="contact-form" action="{{ url('/post') }}" align="center" style="margin: 0px auto;">
        		@csrf
            	<p class="contact-name">
            		<input id="contact_name" type="text" placeholder="Title" value="{{ old('title') ? old('title') : $post->title}}" name="title" /> 
            		@if ($errors->has('title'))
            		<span class="invalid-feedback" role="alert">
            		<strong> {{ $errors->first('title')}}</strong>
            		</span>
            		@endif
                </p>
                <!-- <p class="contact-email">
                	<input id="contact_email" type="text" placeholder="Email Address" value="" name="email" />
                </p> -->
                <p class="contact-message">
                	<textarea id="contact_message"  rows="15" cols="40"placeholder="Your Message" name="body">{{ old ('body') ? old('body') : $post->body}}</textarea>
                	@if ($errors->has('body'))
                	<span class="invalid-feedback" role="alert">
                	<strong> {{ $errors->first('body')}}</strong>
                	</span>
                	@endif

                </p>
                <p class="contact-submit">
                	<input type="submit">
                </p>
                
                <div id="response">
                
                </div>
            </form>
         
        </div>
        
        <!-- <div class="span3">
        	<div class="contact-details">
        		<h3>Contact Details</h3>
                <ul>
                    <li><a href="#">hello@brushed.com</a></li>
                    <li>(916) 375-2525</li>
                    <li>
                        Brushed Studio
                        <br>
                        5240 Vanish Island. 105
                        <br>
                        Unknow
                    </li>
                </ul>
            </div>
        </div> -->
    </div>
    <!-- End Contact Form -->
</div>
</div>
<!-- End Contact Section -->



<!-- Footer -->
<footer>
	<p class="credits">&copy;2013 Brushed. <a href="http://themes.alessioatzeni.com/html/brushed/" title="Brushed | Responsive One Page Template">Brushed Template</a> by <a href="http://www.alessioatzeni.com/" title="Alessio Atzeni | Web Designer &amp; Front-end Developer">Alessio Atzeni</a></p>
</footer>
<!-- End Footer -->

<!-- Back To Top -->
<a id="back-to-top" href="#">
	<i class="font-icon-arrow-simple-up"></i>
</a>
<!-- End Back to Top -->


	
</section>

@endsection