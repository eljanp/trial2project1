@extends('layout.master')

@section('content')

<section>
	<h2>{{ $post->title }}</h2>
	<p> {{ $post->body }}</p>
		

	<h3>Comments</h3>


	@foreach($comments as $comment)

	<p>Author: {{ $comment->user->name }}</p>
	<p> {{ $comment->body }}</p>


	@endforeach
	<form action="{{ url('/comment') }}" method="post">
		@csrf
		<input type="hidden" name="post_id" value="{{ $post->id }}">
		<textarea name="body"></textarea>
		<button type="submit">Submit Comment</button>

	</form>


</section>

@endsection