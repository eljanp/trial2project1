@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>



                <div class="card-body">
                 
                    <h2>Welcome,{{ Auth::user()->email}}</h2>


                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <form action="{{ url('/logout') }}" method="post">
                        @csrf
                        <button type="submit">Log out</button>
                    </form>
             

                </div>                    






                </div>
            </div>
        </div>
    </div>
</div>
@endsection
