<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layout.master');
// });



Route::get('/','PagesController@index');

Route::get('/','PagesController@home')->middleware('mobile');

Route::get('/pages','PagesController@show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'PagesController@logout');

Route::get('/posts', 'PostsController@index');

Route::get('/newpost', 'PostsController@store');

Route::get('post/{id}', 'PostsController@show');

Route::get('post/{id}/edit', 'PostsController@edit');

Route::post('/post', 'PostsController@store');

Route::get('/post', 'PostsController@create');

Route::post('/post/{id}', 'PostsController@update');

Route::post('/comment','PostsController@storeComment');

Route::get('/admin','PagesController@showAdmin')->middleware('authenticated','admin');

// Route::get('/detect','PagesController@detect');

Route::get('admin/{id}/edit', 'PagesController@edit');

Route::get('/pages', 'PagesController@edit');

Route::post('/page/{id}', 'PagesController@update')->middleware('authenticated','admin');

Route::get('/page/{id}', 'PagesController@delete')->middleware('admin');